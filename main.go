package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

type Customers struct {
	Id          string `form:"cust_id" json:"cust_id"`
	Name        string `form:"nama" json:"nama"`
	Address     string `form:"alamat" json:"alamat"`
	PostalCode  string `form:"kode_pos" json:"kode_pos"`
	PhoneNumber string `form:"no_hp" json:"no_hp"`
	Email       string `form:"email" json:"email"`
}

var customers Customers
var arrCustomers []Customers
var db *sql.DB
var err error

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome to the HomePage !")
	fmt.Println("Endpoint Hit: homePage")
}

func returnAllCustomers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var customers Customers
	var arrCustomers []Customers

	result, err := db.Query("SELECT cust_id, nama, alamat, kode_pos, no_hp, email from customer_bank")
	if err != nil {
		log.Print(err)
	}
	defer result.Close()

	for result.Next() {
		err := result.Scan(&customers.Id, &customers.Name, &customers.Address, &customers.PostalCode, &customers.PhoneNumber, &customers.Email)
		if err != nil {
			log.Fatal(err.Error())
		} else {
			arrCustomers = append(arrCustomers, customers)
		}
	}

	fmt.Println("Endpoint Hit: returnAllCustomers")
	json.NewEncoder(w).Encode(arrCustomers)
}

func returnSingleCustomer(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	vars := mux.Vars(r)
	result, err := db.Query("SELECT cust_id, nama, alamat, kode_pos, no_hp, email FROM customer_bank WHERE cust_id =?", vars["cust_id"])
	if err != nil {
		panic(err.Error())
	}
	defer result.Close()

	var customers Customers

	for result.Next() {
		err := result.Scan(&customers.Id, &customers.Name, &customers.Address, &customers.PostalCode, &customers.PhoneNumber, &customers.Email)
		if err != nil {
			log.Fatal(err.Error())
		}
	}
	json.NewEncoder(w).Encode(customers)
}

func createNewCustomer(w http.ResponseWriter, r *http.Request) {

	stmt, err := db.Prepare("INSERT INTO customer_bank(nama,alamat,kode_pos,no_hp,email) VALUES(?)")
	if err != nil {
		panic(err.Error())
	}

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err.Error())
	}
	keyVal := make(map[string]string)
	json.Unmarshal(reqBody, &keyVal)
	Name := keyVal["nama"]
	Address := keyVal["alamat"]
	PostalCode := keyVal["kode_pos"]
	PhoneNumber := keyVal["no_hp"]
	Email := keyVal["email"]

	_, err = stmt.Exec(Name)
	if err != nil {
		panic(err.Error())
	}
	_, err = stmt.Exec(Address)
	if err != nil {
		panic(err.Error())
	}
	_, err = stmt.Exec(PostalCode)
	if err != nil {
		panic(err.Error())
	}
	_, err = stmt.Exec(PhoneNumber)
	if err != nil {
		panic(err.Error())
	}
	_, err = stmt.Exec(Email)
	if err != nil {
		panic(err.Error())
	}

	fmt.Fprintf(w, "New customer was added")
	json.NewEncoder(w).Encode(customers)
}

func deleteCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	stmt, err := db.Prepare("DELETE FROM customer_bank WHERE cust_id=?")
	if err != nil {
		panic(err.Error())
	}
	_, err = stmt.Exec(vars["cust_id"])
	if err != nil {
		panic(err.Error())
	}

	fmt.Fprintf(w, "Post with ID = %s was deleted", vars["cust_id"])
}

func updateDataCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	stmt, err := db.Prepare("UPDATE customer_bank SET nama=? alamat=? kode_pos=? no_hp=? email=? WHERE cust_id = ?")
	if err != nil {
		panic(err.Error())
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err.Error())
	}

	keyVal := make(map[string]string)
	json.Unmarshal(body, &keyVal)
	newName := keyVal["nama"]
	newAddress := keyVal["alamat"]
	newPostalCode := keyVal["kode_pos"]
	newPhoneNumber := keyVal["no_hp"]
	newEmail := keyVal["email"]

	_, err = stmt.Exec(newName, vars["cust_id"])
	if err != nil {
		panic(err.Error())
	}
	_, err = stmt.Exec(newAddress, vars["alamat"])
	if err != nil {
		panic(err.Error())
	}
	_, err = stmt.Exec(newPostalCode, vars["kode_pos"])
	if err != nil {
		panic(err.Error())
	}
	_, err = stmt.Exec(newPhoneNumber, vars["no_hp"])
	if err != nil {
		panic(err.Error())
	}
	_, err = stmt.Exec(newEmail, vars["email"])
	if err != nil {
		panic(err.Error())
	}

	fmt.Fprintf(w, "Post with ID=%s was updated", vars["cust_id"])
}

func main() {
	db, err := sql.Open("mysql", "root:@tcp(localhost:3306)/db_nodejs")

	defer db.Close()
	if err != nil {
		log.Fatal(err)
	}

	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/customers", returnAllCustomers).Methods("GET")
	myRouter.HandleFunc("/customer", createNewCustomer).Methods("POST")
	myRouter.HandleFunc("/customer/{id}", deleteCustomer).Methods("DELETE")
	myRouter.HandleFunc("/customer/{id}", updateDataCustomer).Methods("PUT")
	myRouter.HandleFunc("/customer/{id}", returnSingleCustomer).Methods("GET")

	fmt.Println("Connected to port 9000")
	log.Fatal(http.ListenAndServe(":9000", myRouter))
}
